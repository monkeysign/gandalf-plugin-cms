<?php

namespace Plugins\CMS\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class PostMeta extends Eloquent {

    protected $dates = ['deleted_at'];
    protected $table = 'post_meta';
    protected $fillable = array('id_post', 'meta_key', 'value', 'iso');
    public $incrementing = false;

    public function post() {
        return $this->belongsTo('Plugins\CMS\Models\Post', 'id', 'id_post');
    }

}
