<?php

namespace Plugins\CMS\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostType extends Eloquent {

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'post_type';
    protected $fillable = array('code', 'name', 'icon', 'order');

	/**
	 * @param $item
	 *
	 * @return mixed
	 */
	public static function saveOrUpdate($item) {
		if (!isset($item['id']) || !$item['id']) {
			return PostType::create($item);
		} else {
			PostType::where('id', '=', $item['id'])->update($item);
			return PostType::find($item['id']);
		}
	}

    /**
     * Prende tutte le tassonomie di questo post type
     * @return Array[TaxPostType]
     */
    public function taxonomy() {
        return $this->hasMany('\Plugins\CMS\Models\TaxPostType', 'id_post_type', 'id');
    }

	/**
	 * Prende tutti i post
	 * @return Array[TaxPostType]
	 */
	public function posts() {
		return $this->hasMany('\Plugins\CMS\Models\Post', 'id_post_type', 'id');
	}
    
    /**
     * Prende tutte le categorie di questo post type
     * @return Array[TaxPostType]
     */
    public function category() {
        return $this->hasMany('\Plugins\CMS\Models\CatPostType', 'id_post_type', 'id');
    }
}
