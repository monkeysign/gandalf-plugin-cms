<?php

namespace Plugins\CMS\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class CatPostType extends Eloquent {
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    protected $table = 'category_post_type';
    protected $fillable = array('id_post_type', 'code', 'name', 'id_parent');
    
    public function post_type() {
        return $this->belongsTo('Plugins\CMS\Models\PostType', 'id', 'id_post_type');
    }
    
    public function parent(){
        return $this->belongsTo('Plugins\CMS\Models\CatPostType', 'id_parent','id');
    }
    
    public function posts(){
        return $this->belongsToMany('Plugins\CMS\Models\Post', 'post_category', 'id_category', 'id_post');
    }

	/**
	 * Prende il PostMeta del post secondo una determinata chiave, o restituisce l'intera collection
	 * @param null $key
	 * @param null $isoLingua
	 *
	 * @return $this|mixed
	 */
	public function meta( $key = null, $isoLingua = null ) {
		// Lingua di default
		if ( ! $isoLingua ) {
			$isoLingua = config( 'locale' );
		}
		// Se non setto una chiave restituisco tutti i meta
		if ( ! $key ) {
			return $this->hasMany( 'Plugins\CMS\Models\CatPostTypeMeta', 'id_category_post_type', 'id' )->where( 'iso', $isoLingua );
		}
		// se ho la chiave restituisco il valore di quel meta in quella lingua
		$val =  $this->hasMany( 'Plugins\CMS\Models\CatPostTypeMeta', 'id_category_post_type', 'id' )
		             ->where( 'meta_key', $key )
		             ->where( 'iso', $isoLingua )
		             ->first();
		if($val) return $val->value;
		else return false;
	}

	/**
	 * @param $item
	 *
	 * @return mixed
	 */
	public static function saveOrUpdate($item) {
		if (!isset($item['id']) || !$item['id']) {
			return CatPostType::create($item);
		} else {
			CatPostType::where('id', '=', $item['id'])->update($item);
			return CatPostType::find($item['id']);
		}
	}
}
