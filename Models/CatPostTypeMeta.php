<?php

namespace Plugins\CMS\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class CatPostTypeMeta extends Eloquent {

    protected $dates = ['deleted_at'];
    protected $table = 'category_post_type_meta';
    protected $fillable = array('id_category_post_type', 'meta_key', 'value', 'iso');
    public $incrementing = false;

    public function post() {
        return $this->belongsTo('Plugins\CMS\Models\CatPostType', 'id', 'id_category_post_type');
    }

}
