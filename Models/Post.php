<?php

namespace Plugins\CMS\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Eloquent {

	use SoftDeletes;

	protected $dates = [ 'deleted_at' ];
	protected $table = 'post';
	protected $fillable = array( 'id_post_type', 'permalink', 'state', 'visibility', 'publish_date' );

	public function post_type() {
		return $this->hasOne( 'Plugins\CMS\Models\PostType', 'id', 'id_post_type' );
	}

	/**
	 * Prende il PostMeta del post secondo una determinata chiave, o restituisce l'intera collection
	 * @param null $key
	 * @param null $isoLingua
	 *
	 * @return $this|mixed
	 */
	public function meta( $key = null, $isoLingua = null ) {
		// Lingua di default
		if ( ! $isoLingua ) {
			$isoLingua = config( 'locale' );
		}
		// Se non setto una chiave restituisco tutti i meta
		if ( ! $key ) {
			return $this->hasMany( 'Plugins\CMS\Models\PostMeta', 'id_post', 'id' )->where( 'iso', $isoLingua );
		}
		// se ho la chiave restituisco il valore di quel meta in quella lingua
		$val =  $this->hasMany( 'Plugins\CMS\Models\PostMeta', 'id_post', 'id' )
		            ->where( 'meta_key', $key )
		            ->where( 'iso', $isoLingua )
		            ->first();
		if($val) return $val->value;
		else return false;
	}

	/**
	 * Cerca $q nel meta "title" e "content"
	 * @param $q
	 *
	 * @return mixed
	 */
	public function search($q){
		return Post::join( 'post_meta', 'post.id', '=', 'post_meta.id_post' )
		    ->where( function ( $query ) use ( $q ) {
			    $query->where( 'post_meta.meta_key', '=', 'title' )
			          ->where( 'post_meta.value', 'like', '%' . $q . '%' );
		    } )
		    ->orWhere( function ( $query ) use ( $q ) {
			    $query->where( 'post_meta.meta_key', '=', 'content' )
			          ->where( 'post_meta.value', 'like', '%' . $q . '%' );
		    } )
		    ->distinct( 'post.id' )
		    ->select( 'post.*' );
	}

	/**
	 * L'autore
	 * @return \Models\User
	 */
	public function author() {
		return $this->hasOne( 'Models\User', 'id', 'id_autore' );
	}

	/**
	 * Prendi le categorie
	 * @return \Plugins\CMS\Models\CatPostType
	 */
	public function categories() {
		return $this->belongsToMany( 'Plugins\CMS\Models\CatPostType', 'post_category', 'id_post', 'id_category' );
	}

	/**
	 * Get all media
	 * @return \Plugins\CMS\Models\Media
	 */
	public function media() {
		return $this->belongsToMany( 'Plugins\CMS\Models\Media', 'post_media', 'id_post', 'id_media' )->withPivot( 'tipo_associazione' );
	}

	/**
	 * Salva un nuovo record o fa l'update
	 * @param $item
	 *
	 * @return \Plugins\CMS\Models\Post
	 */
	public static function saveOrUpdate( $item ) {
		if ( ! isset( $item['id'] ) || ! $item['id'] ) {
			return Post::create( $item );
		} else {
			Post::where( 'id', $item['id'] )->update( $item );
			return Post::find( $item['id'] );
		}
	}

}
