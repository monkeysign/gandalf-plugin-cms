<?php

namespace Plugins\CMS\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaxPostType extends Eloquent {
	use SoftDeletes;

	protected $dates = ['deleted_at'];
	protected $table = 'taxonomy_post_type';
	protected $fillable = array('id_post_type','meta_key', 'label', 'type', 'pre_value');

	public function post_type() {
		return $this->belongsTo('Plugins\CMS\Models\PostType', 'id', 'id_post_type');
	}

	public function field(){
		return json_decode($this->pre_value);
	}

	/**
	 * @param $item
	 *
	 * @return mixed
	 */
	public static function saveOrUpdate($item) {
		if (!isset($item['id']) || !$item['id']) {
			return TaxPostType::create($item);
		} else {
			TaxPostType::where('id', '=', $item['id'])->update($item);
			return TaxPostType::find($item['id']);
		}
	}
}