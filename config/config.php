<?php
return [
	"name"   => 'cms',
	"class"  => \Plugins\CMS\Plugin::class,
	"routes" => __DIR__ . '/../routes/routes.php'
];