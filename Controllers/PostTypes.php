<?php

namespace Plugins\CMS\Controllers;

use Modules\Backend\Classes\Controller;
use Plugins\CMS\Models\PostType;

class PostTypes extends Controller {

	//Lista
	public function listAll() {
		// Tutti i user registrati
		$param['table'] = PostType::all();
		return view()->render( 'cms.posttype.list', $param );
	}

	//Form Add
	public function form( $id = null ) {
		if ( isset( $id ) && $id ) {
			$param['record'] = PostType::find( $id );
		} else {
			$param['record'] = new PostType();
		}

		return view()->render( 'cms.posttype.form', $param );
	}

	//Delete single
	public function delete( $id = null ) {
		$user = PostType::find( $id );
		$user->delete();
		//usare forceDelete() solo se si vuole una cancellazione fisica
		$data = array( 'result' => true );
		return $data;
	}

	//Delete group
	public function deleteGroup() {
		// $_POST['ids']
		$group = request()->get( 'ids' );
		PostType::whereIn( 'id', $group )->delete();
		$data = array( 'result' => true );
		return $data;
	}

	//Save
	public function save() {
		$item      = request()->get('item');
		//$validator = new \Modules\Backend\Classes\ValidationUsers();

		try {
			//$validator->validate( $item );
			$record = PostType::saveOrUpdate( $item );
			$param  = [
				'record' => $record,
				'state'  => true,
				'mex'    => 'Salvataggio Riuscito'
			];
		} catch ( \Ring\Exception\ValidationException $ex ) {
			die( $ex->getMessage() );
		}

		return $param;
	}

}