<?php

namespace Plugins\CMS\Controllers;

use Modules\Backend\Classes\Controller;
use Plugins\CMS\Models\TaxPostType;
use Plugins\CMS\Models\PostType;

class TaxPostTypes extends Controller {

	private $param;

	//constructor
	public function __construct( \Illuminate\Http\Request $request ) {
		parent::__construct( $request );
		$this->param['code_post_type'] = request()->code_post_type;
		$this->param['postType'] = PostType::where('code', $this->param['code_post_type'])->firstOrFail();
	}

	//Lista
	public function listAll() {
		$this->param['table'] = TaxPostType::where('id_post_type', $this->param['postType']->id)->orderBy('created_at', 'ASC')->get();
		return view()->render( 'cms.taxonomy.list', $this->param );
	}

	//Form Add
	public function form( $code='', $id = null ) {
		if ( isset( $id ) && $id ) {
			$this->param['record'] = TaxPostType::find( $id );
		} else {
			$this->param['record'] = new TaxPostType();
		}
		return view()->render( 'cms.taxonomy.form', $this->param );
	}

	//Delete single
	public function delete( $code='', $id = null ) {
		$user = TaxPostType::find( $id );
		$user->delete();
		//usare forceDelete() solo se si vuole una cancellazione fisica
		$data = array( 'result' => true );

		return $data;
	}

	//Delete group
	public function deleteGroup() {
		// $_POST['ids']
		$group = request()->get( 'ids' );
		TaxPostType::whereIn( 'id', $group )->delete();
		$data = array( 'result' => true );

		return $data;
	}

	//Save
	public function save() {
		$item = request()->get( 'item' );
		//$validator = new \Modules\Backend\Classes\ValidationUsers();

		try {
			//$validator->validate( $item );
			$record = TaxPostType::saveOrUpdate( $item );
			$param  = [
				'record' => $record,
				'state'  => true,
				'mex'    => 'Salvataggio Riuscito'
			];
		} catch ( \Ring\Exception\ValidationException $ex ) {
			die( $ex->getMessage() );
		}

		return $param;
	}
}