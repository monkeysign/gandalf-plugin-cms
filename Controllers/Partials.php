<?php

namespace Plugins\CMS\Controllers;

use Illuminate\Http\File;
use Modules\Backend\Classes\Controller;
use Plugins\CMS\Models\PostType;

class Partials extends Controller {

	//Lista
	public function listAll() {
		// Tutti i user registrati
		$allPartial = glob( __DIR__ . '/../views/cms/partials/*.php' );
		$records    = [];
		foreach ( $allPartial as $file ) {
			$obj           = new \stdClass();
			$obj->filename = basename( $file );
			$records[]     = $obj;
		}
		$param['table'] = $records;

		return view()->render( 'cms.partialmanage.list', $param );
	}

	//Form Add
	public function form( $id = null ) {
		if ( isset( $id ) && $id ) {
			$obj             = new \stdClass();
			$obj->filename   = basename( $id );
			$obj->content    = file_get_contents( __DIR__ . '/../views/cms/partials/' . $id );
			$param['record'] = $obj;
		} else {
			$param['record']           = new \stdClass();
			$param['record']->id       = null;
			$param['record']->filename = null;
			$param['record']->content  = null;
		}

		return view()->render( 'cms.partialmanage.form', $param );
	}

	//Delete single
	public function delete( $id = null ) {
		unlink(__DIR__ . '/../views/cms/partials/' . $id);
		$data = array( 'result' => true );
		return $data;
	}


	//Save
	public function save() {
		$item = request()->get( 'item' );
		//$validator = new \Modules\Backend\Classes\ValidationUsers();

		try {
			if($item['id'])
				unlink(__DIR__ . '/../views/cms/partials/' . $item['id']);
			else {
				$item['id'] = $item['filename'];
			}
			$file = fopen( __DIR__ . '/../views/cms/partials/' . $item['filename'], 'w+' );
			file_put_contents( __DIR__ . '/../views/cms/partials/' . $item['filename'], $item['content'] );
			$param = [
				'record' => $item,
				'state'  => true,
				'mex'    => 'Salvataggio Riuscito'
			];

		} catch ( \Exception $ex ) {
			die( $ex->getMessage() );
		}

		return $param;
	}

}