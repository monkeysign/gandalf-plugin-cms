<?php

namespace Plugins\CMS\Controllers;

use Modules\Backend\Classes\Controller;
use Plugins\CMS\Models\PostType;
use Plugins\CMS\Models\PostMeta;
use Plugins\CMS\Models\CatPostType;
use Plugins\CMS\Models\Post;

class Posts extends Controller {

	private $param;

	//constructor
	public function __construct( \Illuminate\Http\Request $request ) {
		parent::__construct( $request );
		$this->param['code_post_type'] = request()->code_post_type;
		$this->param['postType']       = PostType::where( 'code', $this->param['code_post_type'] )->firstOrFail();
	}

	//Lista
	public function listAll() {
		$this->param['table'] = Post::where( 'id_post_type', $this->param['postType']->id )->orderBy( 'created_at', 'DESC' )->get();

		return view()->render( 'cms.post.list', $this->param );
	}

	//Form Add
	public function form( $code = '', $id = null ) {
		$this->param['allCat'] = CatPostType::where( 'id_post_type', $this->param['postType']->id )->orderBy( 'name', 'ASC' )->get();
		if ( isset( $id ) && $id ) {
			$this->param['record']    = Post::find( $id );
			$this->param['catRecord'] = [];
			foreach ( $this->param['record']->categories as $cat ) {
				$this->param['catRecord'][] = $cat->id;
			}
		} else {
			$this->param['catRecord'] = [];
			$this->param['record']    = new Post();
		}

		return view()->render( 'cms.post.form', $this->param );
	}

	//Delete single
	public function delete( $code = '', $id = null ) {
		$post = Post::find( $id );
		PostMeta::where( 'id_post', $post->id )->delete();
		$post->delete();
		//usare forceDelete() solo se si vuole una cancellazione fisica
		$data = array( 'result' => true );

		return $data;
	}

	//Delete group
	public function deleteGroup() {
		// $_POST['ids']
		$group = request()->get( 'ids' );
		Post::whereIn( 'id', $group )->delete();
		$data = array( 'result' => true );

		return $data;
	}

	//Save
	public function save() {
		$item = request()->get( 'item' );
		//$validator = new \Modules\Backend\Classes\ValidationUsers();

		try {
			//$validator->validate( $item );
			$record = Post::saveOrUpdate( $item );

			//save category
			if ( request()->get( 'categories' ) ) {
				$cat = request()->get( 'categories' );
				$record->categories()->detach();
				$record->categories()->attach( $cat );
			} else {
				$record->categories()->detach();
			}

			//save meta
			$this->savePostMeta( $record );

			//permalink
			if ( ! $record->permalink ) {
				if ( $record->meta( 'title' ) ) {
					$record->permalink = str_replace( ' ', '-', strtolower( trim( preg_replace( "/[^a-zA-Z0-9 ]/", '', $record->meta( 'title' ) ) ) ) );
				} else {
					$record->permalink = $this->param['postType']->id . '-' . rand( 0, 1000000 );
				}
			}

			//data_pubblicazione
			if ( ! $record->publish_date ) {
				$record->publish_date = date( "Y-m-d H:i" );
			}

			// salvo
			$record->update();

			// hook
			hooks()->do_action( CMS_ADMIN_POST_SAVE, $record );

			$param = [
				'record' => $record,
				'state'  => true,
				'mex'    => 'Salvataggio Riuscito'
			];

		} catch ( \Ring\Exception\ValidationException $ex ) {
			die( $ex->getMessage() );
		}

		return $param;
	}

	/**
	 * Upload image for gallery
	 * @return array
	 * @throws \Psr\Container\ContainerExceptionInterface
	 * @throws \Psr\Container\NotFoundExceptionInterface
	 */
	public function galleryUpload() {
		foreach ( $_FILES as $key => $file ) {
			$file["name"] = date( 'U' ) . '_' . $file["name"];
			if ( ! move_uploaded_file( $file['tmp_name'], config( 'media' ) . '/' . $file['name'] ) ) {
				$param = [
					'state' => false,
				];
			} else {
				$param = [
					'state'    => true,
					'file'     => config( 'httpmedia' ) . '/' . $file['name'],
					'filename' => $file['name']
				];
			}

			return $param;
		}
	}

	/**
	 * Delete a file
	 * @throws \Psr\Container\ContainerExceptionInterface
	 * @throws \Psr\Container\NotFoundExceptionInterface
	 */
	public function galleryDelete() {
		$items = request()->get( 'filename' );
		unlink( config( 'media' ) . '/' . $items );
	}

	/**
	 * Salva i meta del post
	 *
	 * @param Post $post
	 */
	public function savePostMeta( \Plugins\CMS\Models\Post $post ) {
		$items = request()->get( 'meta' );

		if ( request()->get( 'language' ) && request()->get( 'language' ) !== '' ) {
			$iso = request()->get( 'language' );
		} else {
			$iso = config( 'locale' );
		}

		// Immagini
		foreach ( $_FILES as $key => $file ) {
			if ( $key === 'files' ) {
				continue;
			}
			if ( $file['name'] ) {
				$file["name"] = date( 'U' ) . '_' . $file["name"];
				if ( ! move_uploaded_file( $file['tmp_name'], config( 'media' ) . '/' . $file['name'] ) ) {
					echo 'error file upload';
				}
				$oldValue = PostMeta::where( 'id_post', $post->id )->where( 'iso', $iso )->where( 'meta_key', $key )->first();
				if ( $oldValue ) {
					unlink( config( 'media' ) . '/' . $oldValue->value );
					PostMeta::where( 'id_post', $post->id )->where( 'iso', $iso )->where( 'meta_key', $key )->delete();
				}
				$metaArray['meta_key'] = $key;
				$metaArray['value']    = $file['name'];
				$metaArray['id_post']  = $post->id;
				$metaArray['iso']      = $iso;
				PostMeta::create( $metaArray );
			} else {
				if ( isset( $_POST[ $key . '-old' ] ) && $_POST[ $key . '-old' ] == '' ) {
					$oldValue = PostMeta::where( 'id_post', $post->id )->where( 'iso', $iso )->where( 'meta_key', $key )->first();
					if ( $oldValue ) {
						if ( file_exists( config( 'media' ) . '/' . $oldValue->value ) ) {
							unlink( config( 'media' ) . '/' . $oldValue->value );
						}
						PostMeta::where( 'id_post', $post->id )->where( 'iso', $iso )->where( 'meta_key', $key )->delete();
					}
				}
			}
		}

		// altri meta
		foreach ( $items as $key => $value ) {
			// Cancello le tassonomie associate
			PostMeta::where( 'id_post', '=', $post->id )->where( 'iso', $iso )->where( 'meta_key', $key )->delete();

			$metaArray['meta_key'] = $key;
			if ( is_array( $items[ $key ] ) ) {
				$metaArray['value'] = serialize( $value );
			} else {
				$metaArray['value'] = $value;
			}
			$metaArray['id_post'] = $post->id;
			$metaArray['iso']     = $iso;
			PostMeta::create( $metaArray );
		}
	}
}