<?php
/**
 * ADMIN HOOKS
 */
//POST
const CMS_ADMIN_POST_FORM_LEFT     = 'cms_admin_post_form_left';
const CMS_ADMIN_POST_FORM_RIGHT    = 'cms_admin_post_form_right';
const CMS_ADMIN_POST_SAVE          = 'cms_admin_post_save';
// CATEGORY POST
const CMS_ADMIN_POST_CATEGORY_FORM = 'cms_admin_post_category_form';
const CMS_ADMIN_POST_CATEGORY_SAVE = 'cms_admin_post_category_save';

/*
 * ----------------------------------
 * HOOK ESTERNI
 * -----------------------------------
 */
/**
 * Aggiungo il path delle views di questo plugin grazie al filtro "views_path" che si trova nel controller generale
 */
hooks()->add_filter( APP_VIEWS_PATH, function ( $views = [] ) {
	return array_merge( [ __DIR__ . '/views/' ], $views );
} );

/**
 * Aggiungo le voci al menu tramite una vista presente nel plugin
 */
hooks()->add_action( ADMIN_LEFT_MENU, function () {
    if(user_logged()->role <= 1) {
        $sidebarPostType = Plugins\CMS\Models\PostType::orderBy('order', 'asc')->get();
        view()->render('cms.left', ['postTypes' => $sidebarPostType]);
    }
},20 );

/**
 * Aggiungo widget alla dashboard
 */
hooks()->add_action( ADMIN_DASHBOARD_WIDGET, function () {
	$pageCount = \Plugins\CMS\Models\Post::where( 'id_post_type', 1 )->get()->count();
	$postCount = \Plugins\CMS\Models\Post::where( 'id_post_type', 2 )->get()->count();
	view()->render( 'cms.dashboard', [ 'postCount' => $postCount, 'pageCount' => $pageCount ] );
},20 );

/**
 * Includo gli helper
 */
require __DIR__ . '/helpers.php';