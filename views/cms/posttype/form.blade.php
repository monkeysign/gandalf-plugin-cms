<?php

use \Ring\Support\Enum\Role;
use \Ring\Support\Enum\Status;

?>
@extends('layout')

@section('content')

    <!-- PAGE CONTENT WRAPPER -->
    <div id="page-wrapper">
        <div class="container-fluid">

            @include('cms.posttype.header')
            <form id="form-save" action="{{ path_for('admin.cms.posttype.save') }}" method="POST"
                  enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h2>Post Type</h2>
                            <hr/>

                            <input id="item_id_hidden" type="hidden"
                                   value='@if($record->id){{ $record->id }}@else{{0}}@endif' name='item[id]'>
                            <div class="form-body">
                                <div class="row">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Codice *</label>
                                            <small><i>(il codice "pages" identifica le pagine del sito)</i></small>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-bookmark"></i>
                                                </div>
                                                <input value="{{$record->code}}" required
                                                       data-toggle="validator" type="text" name="item[code]"
                                                       id="code" class="form-control"
                                                       placeholder="Codice">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>


                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Campo Obbligatorio"
                                                   class="control-label">Nome *</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-bookmark"></i>
                                                </div>
                                                <input value="{{$record->name}}" required
                                                       data-toggle="validator" type="text" name="item[name]"
                                                       id="name" class="form-control"
                                                       placeholder="Nome Visualizzato">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Icona"
                                                   class="control-label">Icona </label>
                                            <small><i>(zmdi zmdi-copy zmdi-hc-fw fa-fw)</i></small>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-star"></i>
                                                </div>
                                                <input value="{{$record->icon}}" type="text"
                                                       name="item[icon]" id="icon" class="form-control"
                                                       placeholder="Icona Visualizzata">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->
                                <div class="row">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label
                                                    title="Campo obbligatorio per la registrazione"
                                                    class="control-label">Ordine</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-exchange-vertical"></i>
                                                </div>
                                                <input
                                                        type="number" name="item[order]" id="order"
                                                        class="form-control" value="{{$record->order}}"
                                                        placeholder="Ordine">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->


                            </div>
                            <!--/row-->

                            <!--/span-->
                            <div class="form-actions">
                                <hr style="margin-top: 0px">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salva
                                </button>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('style')
    <link href="{{asset('assets/plugins/bower_components/toast-master/css/jquery.toast.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/custom-select/custom-select.css')}}" rel="stylesheet"
          type="text/css">
@endsection

@section('scripts')
    <script src="{{asset('assets/js/validator.js')}}"></script>
    <script src="{{asset('assets/js/form.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/blockUI/jquery.blockUI.js')}}"></script>

    <!-- Select Plugin JavaScript -->
    <script src="{{asset('assets/plugins/bower_components/custom-select/custom-select.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js')}}"
            type="text/javascript"></script>

    <script>
        // Select Choosen
        jQuery(document).ready(function () {
            $(".select-choose").select2();
        });
    </script>
@endsection