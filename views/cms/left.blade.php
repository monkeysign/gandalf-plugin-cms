<li class="nav-small-cap m-t-10">--- CMS</li>
@if( user_logged()->role == 0 )
    <li>
        <a href="{{ path_for('admin.cms.posttype.base') }}" class="waves-effect"><i
                    class="zmdi zmdi-code-setting zmdi-hc-fw fa-fw"></i> <span class="hide-menu"> Post Type <span
                        class="fa arrow"></span></span></a>
        <ul class="nav nav-second-level">
            <li><a href="{{path_for('admin.cms.posttype.list')}}">Vai alla lista</a></li>
            <li><a href="{{path_for('admin.cms.posttype.add')}}">Aggiungi Nuovo</a></li>
        </ul>
    </li>
    <li>
        <a href="{{ path_for('admin.cms.partial.base') }}" class="waves-effect"><i
                    class="zmdi zmdi-code zmdi-hc-fw fa-fw"></i> <span class="hide-menu"> Parziali <span
                        class="fa arrow"></span></span></a>
        <ul class="nav nav-second-level">
            <li><a href="{{path_for('admin.cms.partial.list')}}">Vai alla lista</a></li>
            <li><a href="{{path_for('admin.cms.partial.add')}}">Aggiungi Nuovo</a></li>
        </ul>
    </li>
@endif

@foreach( $postTypes as $postType )
<li>
<a href="{{ path_for('admin.cms.singleposttype.base', ['code_post_type' => $postType->code ]) }}" class="waves-effect">
    <i class="{{$postType->icon}}"></i> <span class="hide-menu"> {{$postType->name}} <span
                class="fa arrow"></span></span></a>
<ul class="nav nav-second-level">
    @if ($postType->code != 'pages')
    <li>
        <a href="{{ path_for('admin.cms.catposttype.list', ['code_post_type' => $postType->code ])}}">Categorie</a>
    </li>
    @endif
    <li>
        <a href="{{ path_for('admin.cms.post.list', ['code_post_type' => $postType->code ]) }}">Visualizza Lista</a>
    </li>
    <li>
        <a href="{{ path_for('admin.cms.post.add', ['code_post_type' => $postType->code ]) }}">Aggiungi Nuovo</a>
    </li>
</ul>
</li>
@endforeach
