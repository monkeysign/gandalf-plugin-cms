@extends('layout')

@section('content')

    <!-- PAGE CONTENT WRAPPER -->
    <div id="page-wrapper">
        <div class="container-fluid">

            @include('cms.taxonomy.header')
            <form id="form-save"
                  action="{{ path_for('admin.cms.taxonomy.save', ['code_post_type' => $code_post_type]) }}"
                  method="POST"
                  enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h2>Tassonomia {{$postType->name}}</h2>
                            <hr/>

                            <input id="item_id_hidden" type="hidden"
                                   value='@if($record->id){{ $record->id }}@else{{0}}@endif' name='item[id]'>
                            <input type="hidden" value='{{ $postType->id }}' name='item[id_post_type]'>
                            <div class="form-body">
                                <div class="row">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Meta Key *</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-bookmark"></i>
                                                </div>
                                                <input value="{{$record->meta_key}}" required
                                                       data-toggle="validator" type="text" name="item[meta_key]"
                                                       id="meta_key" class="form-control"
                                                       placeholder="Meta Key">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>


                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Campo Obbligatorio"
                                                   class="control-label">Label *</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-bookmark"></i>
                                                </div>
                                                <input value="{{ $record->label }}" required
                                                       data-toggle="validator" type="text" name="item[label]"
                                                       id="label" class="form-control"
                                                       placeholder="Label Campo">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Catergoria Madre"
                                                   class="control-label">Tipo Campo </label>
                                            <div class="input-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="ti-settings"></i>
                                                    </div>
                                                    <select name="item[type]" class="form-control select-choose">
                                                        <option value="">- Seleziona il tipo -</option>
                                                        <option @if($record->type == 0)
                                                                selected
                                                                @endif value="0">
                                                            Text
                                                        </option>
                                                        <option @if($record->type == 1)
                                                                selected
                                                                @endif value="1">
                                                            Select
                                                        </option>
                                                        <option @if($record->type == 2)
                                                                selected
                                                                @endif value="2">
                                                            Textarea
                                                        </option>
                                                        <option @if($record->type == 3)
                                                                selected
                                                                @endif value="3">
                                                            Upload Immagine
                                                        </option>
                                                        <option @if($record->type == 4)
                                                                selected
                                                                @endif value="4">
                                                            Gallery
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="pre_value" class="control-label">Valori Prefissati (json) solo
                                                Per tipo select</label>
                                            <pre>
<code>
Esempio:
    {"data":[
        {"key":"Label Option", "value":"Valore 1"},
        {"key":"Altra Label", "value":"Valore 2"}
    ]}
</code>
                        </pre>
                                            <textarea id="pre_value" name="item[pre_value]" class="form-control"
                                                      style="height:250px">{{ $record->pre_value }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/row-->

                            <!--/span-->
                            <div class="form-actions">
                                <hr style="margin-top: 0px">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salva
                                </button>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{asset('assets/plugins/bower_components/toast-master/css/jquery.toast.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/custom-select/custom-select.css')}}" rel="stylesheet"
          type="text/css">
@endsection

@section('scripts')
    <script src="{{asset('assets/js/validator.js')}}"></script>
    <script src="{{asset('assets/js/form.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/blockUI/jquery.blockUI.js')}}"></script>

    <!-- Select Plugin JavaScript -->
    <script src="{{asset('assets/plugins/bower_components/custom-select/custom-select.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js')}}"
            type="text/javascript"></script>

    <script>
        // Select Choosen
        jQuery(document).ready(function () {
            $(".select-choose").select2();
        });
    </script>
@endsection