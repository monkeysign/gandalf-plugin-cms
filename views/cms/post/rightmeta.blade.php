@foreach($postType->taxonomy as $tax)
    @if($tax->type == 3)
        <div class="row mt-4 mb-4">
            <div class="col-12">
                <label for="input-file-now">{{$tax->label}}</label>
                <input type="file" id="{{$tax->meta_key}}" name="{{$tax->meta_key}}"
                       {{--data-allowed-file-extensions="jpg jpeg png gif"--}}
                       class="dropify"
                       data-default-file="@if($record->meta($tax->meta_key)){{config('httpmedia').$record->meta($tax->meta_key)}}@else{{''}}@endif"/>
                <input id="{{$tax->meta_key}}-old" type="hidden" name="{{$tax->meta_key}}-old"
                       value="{{$record->meta($tax->meta_key)}}">
                <script>
                    $(function () {
                        // upload
                        var __{{$tax->meta_key}} = $("#{{$tax->meta_key}}").dropify();
                        __{{$tax->meta_key}}.on('dropify.afterClear', function (event, element) {
                            $('#{{$tax->meta_key}}-old').val('');
                        });
                    })
                </script>
            </div>
        </div>
    @endif
@endforeach