@extends('layout')

@section('content')

    <!-- PAGE CONTENT WRAPPER -->
    <div id="page-wrapper">
        <div class="container-fluid">

            @include('cms.post.header')

            <form id="form-save"
                  action="{{ path_for('admin.cms.post.save', ['code_post_type' => $code_post_type]) }}"
                  method="POST"
                  enctype="multipart/form-data">
                <input id="item_id_hidden" type="hidden"
                       value='@if($record->id){{ $record->id }}@else{{0}}@endif' name='item[id]'>
                <input type="hidden" value='{{ $postType->id }}' name='item[id_post_type]'>

                <div class="row">
                    <div class="col-md-9">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label title="Campo obbligatorio"
                                               class="control-label">Titolo</label>
                                        <div class="input-group">
                                            <input value="{{$record->meta('title')}}"
                                                   data-toggle="validator" type="text" name="meta[title]"
                                                   id="title" class="form-control"
                                            >
                                        </div>
                                        <span class="help-block with-errors"> </span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <label>Contenuto</label>
                                    <div>
                                        <div data-field="#meta-content"
                                             class="summernote">{!! $record->meta('content') !!}</div>
                                        <input id="meta-content" type="hidden" name="meta[content]"
                                               value="{{$record->meta('content')}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="white-box">
                                        <h2>SOCIAL</h2>
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="input-file-now">Immagine Social</label>
                                                <input type="file" id="imgsocial" name="imgsocial"
                                                       {{--data-allowed-file-extensions="jpg jpeg png gif"--}}
                                                       class="dropify"
                                                       data-default-file="@if($record->meta('imgsocial')){{config('httpmedia')."".$record->meta('imgsocial')}}@else{{''}}@endif"/>
                                                <input id="imgsocial-old" type="hidden" name="imgsocial-old"
                                                       value="{{$record->meta('imgsocial')}}">
                                                <script>
                                                    $(function () {
                                                        // upload
                                                        var __imghighlight = $("#imgsocial").dropify();
                                                        __imghighlight.on('dropify.afterClear', function (event, element) {
                                                            $('#imgsocial-old').val('');
                                                        });
                                                    })
                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @include('cms.post.meta')

                        {{-- MEDIA QUI --}}

                        <!--/row-->


                        </div>
                        <!--/row-->

                        {{--- HOOK FORM --}}
                        {{ hooks()->do_action(CMS_ADMIN_POST_FORM_LEFT, $record) }}

                    </div>

                    @include('cms.post.right')
                    {{--- HOOK FORM --}}
                    {{ hooks()->do_action(CMS_ADMIN_POST_FORM_RIGHT, $record) }}

                </div>

            </form>

            @yield('modal')

        </div>
    </div>

@endsection

@section('style')
    <link href="{{asset('assets/plugins/bower_components/toast-master/css/jquery.toast.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/custom-select/custom-select.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/summernote/dist/summernote.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/dropify/dist/css/dropify.min.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/codemirror/lib/codemirror.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/codemirror/theme/dracula.css')}}" rel="stylesheet"
          type="text/css">
@endsection

@section('scripts')
    <script src="{{asset('assets/js/validator.js')}}"></script>
    <script src="{{asset('assets/js/form.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/blockUI/jquery.blockUI.js')}}"></script>


    <!-- Select Plugin JavaScript -->
    <script src="{{asset('assets/plugins/bower_components/custom-select/custom-select.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js')}}"
            type="text/javascript"></script>

    <!-- cedemirror -->
    <script src="{{asset('assets/plugins/bower_components/codemirror/lib/codemirror.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/mode/xml/xml.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/mode/javascript/javascript.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/mode/css/css.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/mode/htmlmixed/htmlmixed.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/mode/php/php.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/addon/edit/matchbrackets.js')}}"></script>

    <!-- Summernote -->
    <script src="{{asset('assets/plugins/bower_components/summernote/dist/summernote.min.js')}}"></script>

    <!-- Dropify -->
    <script src="{{asset('assets/plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>

    <script>
        // Select Choosen
        jQuery(document).ready(function () {

            // select singole
            $(".select-choose").select2();
            // file upload
            $('.select2-multiple').select2();

            $.ajax({
                url: 'https://api.github.com/emojis',
                async: false
            }).then(function (data) {
                window.emojis = Object.keys(data);
                window.emojiUrls = data;
            });
            ;


            $('.summernote').summernote({
                height: 350, // set editor height
                minHeight: null, // set minimum height of editor
                maxHeight: null, // set maximum height of editor
                codemirror: { // codemirror options
                    lineNumbers: true,
                    theme: 'dracula',
                    mode: "php"
                },
                hint: {
                    match: /:([\-+\w]+)$/,
                    search: function (keyword, callback) {
                        callback($.grep(emojis, function (item) {
                            return item.indexOf(keyword) === 0;
                        }));
                    },
                    template: function (item) {
                        var content = emojiUrls[item];
                        return '<img src="' + content + '" width="20" /> :' + item + ':';
                    },
                    content: function (item) {
                        var url = emojiUrls[item];
                        if (url) {
                            return $('<img />').attr('src', url).css('width', 20)[0];
                        }
                        return '';
                    }
                },
                focus: false // set focus to editable area after initializing summernote
            });

            // summernote.change
            $('.summernote').on('summernote.change', function (we, contents, $editable) {
                $($(this).data('field')).val($(this).summernote('code'));
            });
            // al click del tasto salva...
            $('button[type="submit"]').on('click', function () {
                $('.summernote').each(function (summernote) {
                    $($(this).data('field')).val($(this).summernote('code'));
                })
                return true;
            })
            //abilito il change del perma o della data
            $('*[data-enable]').on('click', function () {
                $(this).hide();
                $($(this).data('enable')).show().removeAttr('disabled');
            })
        });
    </script>
@endsection