@foreach($postType->taxonomy as $tax)
    @if($tax->type == 0)
        {{-- TEXT --}}
        <div class="row mt-5 mb-5">
            <div class="col-12">
                <label for="{{$tax->meta_key}}" class="control-label">{{$tax->label}}</label>
                <div>
                    <input type="text" id="{{$tax->meta_key}}" name="meta[{{$tax->meta_key}}]" class="form-control"
                           placeholder="Titolo" value="{{$record->meta($tax->meta_key)}}">
                </div>
            </div>
        </div>
    @elseif($tax->type == 1)
        {{-- SELECT --}}
        <div class="row mt-5 mb-5">
            <div class="col-12">
                <label for="{{$tax->meta_key}}" class="control-label">{{$tax->label}}</label>
                <div>
                    <select name="meta[{{$tax->meta_key}}]" class="select2 select-choose form-control">
                        @foreach($tax->field()->data as $field)
                            <option @if($record->meta($tax->meta_key)== $field->value){{"selected"}}@endif value="{{$field->value}}">{{$field->key}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    @elseif($tax->type == 2)
        {{-- TEXTAREA --}}
        <div class="row mt-5 mb-5">
            <div class="col-12">
                <label for="{{$tax->meta_key}}" class="control-label">{{$tax->label}}</label>
                <div>
                    <div data-field="#meta-{{$tax->meta_key}}"
                         class="summernote">{!! $record->meta($tax->meta_key) !!}</div>
                    <input id="meta-{{$tax->meta_key}}" type="hidden"
                           value="{{$record->meta($tax->meta_key)}}" name="meta[{{$tax->meta_key}}]">
                </div>
            </div>
        </div>
    @elseif($tax->type == 4)
        {{-- GALLERY --}}
        <div class="row mt-4 mb-4">
            <h3 class="col-12 mb-5">{{$tax->label}}
                <button data-toggle="modal" data-target=".bs-example-modal-sm" type="button"
                        class="ml-3 btn btn-primary" id="add-galley-item">Aggiungi alla gallery
                </button>
            </h3>
            <div id="{{$tax->meta_key}}-cont" class="row">
                <div class="flex-gallery ">
                    <input name="meta[{{$tax->meta_key}}]" type="hidden" value="">
                    @if($record->meta($tax->meta_key))
                        @foreach(unserialize($record->meta($tax->meta_key)) as $media)
                            <div class="flex-item">
                                <img src="{{ config('httpmedia'). basename($media) }}" class="mw-100">
                                <input name="meta[{{$tax->meta_key}}][]" type="hidden" value="{{basename($media)}}">
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        {{-- Piazzo un modal per l'upload in ajax dei file --}}
@section('modal')
    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel">Upload File</h4>
                </div>
                <div class="modal-body">
                    <form id="{{$tax->meta_key}}_form"
                          action="{{ path_for('admin.cms.post.galleryupload', ['code_post_type' => $code_post_type]) }}"
                          method="POST"
                          enctype="multipart/form-data">

                        <input type="file" id="{{$tax->meta_key}}_input" name="{{$tax->meta_key}}"
                               {{--data-allowed-file-extensions="jpg jpeg png gif"--}}
                               class="dropify"
                        />
                    </form>
                    {{--<div class="flex-gallery flex-selection">

                        @foreach(media_gallery() as $media)
                            <div class="flex-item">
                                <img src="{{ config('httpmedia'). basename($media) }}" class="mw-100">
                            </div>
                        @endforeach
                    </div>--}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Chiudi
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <script>
        $(function () {

            $('#{{$tax->meta_key}}_input').dropify().on('change', function () {
                $('#{{$tax->meta_key}}_form').submit();
            });


            $('#{{$tax->meta_key}}_form').on('submit', function (e) {
                var $form = $('#{{$tax->meta_key}}_form')
                var formData = new FormData($form[0]);

                $.ajax({
                    method: "POST",
                    url: $form.attr("action"),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data.state === true) {
                            $('#{{$tax->meta_key}}-cont .flex-gallery').append(
                                '<div class="flex-item">\n' +
                                '<img src="' + data.file + '" class="mw-100">\n' +
                                '<input name="meta[{{$tax->meta_key}}][]" type="hidden" value="' + data.filename + '">\n' +
                                '</div>'
                            )
                        }
                        $('#{{$tax->meta_key}}_form .dropify-clear').click();
                        $('#form-save').submit();
                    }
                });
                return false;
            })

            $(document).on('click', '.flex-gallery .flex-item', function () {
                var el = $(this)
                $.ajax({
                    method: "POST",
                    url: '{{ path_for('admin.cms.post.gallerydelete', ['code_post_type' => $code_post_type]) }}',
                    data: {'filename': el.find('input').val()},
                    success: function () {
                        el.remove();
                        $('#form-save').submit();
                    }
                })
            })

        });
    </script>
@endsection
@endif
@endforeach