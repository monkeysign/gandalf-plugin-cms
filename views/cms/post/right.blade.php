<div class="col-md-3">
    <div class="white-box">

        @if($record->id)
            <div class="row mb-4">
                <div class="col-12">
                    <label for="permalink" class="control-label">Permalink:</label>
                    @if(!$record->permalink)
                        <div>
                            <input data-toggle="validator" type="text" id="permalink"
                                   name="item[permalink]"
                                   class="form-control">
                        </div>
                    @else
                        <div>{{$record->permalink}}</div>

                        <div class="mt-2 mb-2">
                            <a href="#" data-enable="#permalink">Modifica</a>
                        </div>

                        <div>
                            <input style="display:none;" type="text" disabled="disabled"
                                   id="permalink"
                                   name="item[permalink]" class="form-control">
                        </div>
                    @endif
                </div>
            </div>
        @endif

        <div class="row mb-4">
            <div class="col-12">
                <label for="state" class="control-label">Stato Pubblicazione</label>
                <select name="item[state]" required data-placeholder="Seleziona lo stato"
                        class="form-control select-choose select2">
                    <option @if($record->state == 0){{"selected"}}@endif value="0">Bozza</option>
                    <option @if($record->state == 1){{"selected"}}@endif value="1">Pubblicato
                    </option>
                </select>
            </div>
        </div>

        <div class="row mb-4 mt-4">
            <div class="col-12">
                <label for="visibility" class="control-label">Visibilit&agrave;</label>
                <select name="item[visibility]" required
                        data-placeholder="Seleziona la visibilit&agrave;"
                        class="form-control select-choose select2">
                    <option @if($record->visibility == 0){{"selected"}}@endif value="0">Pubblico
                    </option>
                    <option @if($record->visibility == 1){{"selected"}}@endif value="1">Privato
                    </option>
                </select>
            </div>
        </div>

        @if($record->id)
            <div class="row mt-4 mb-4">
                <div class="col-12">
                    <label for="publish_date" class="control-label">Pubblicato il:</label>
                    @if(!$record->publish_date)
                        <div>
                            <input type="datetime-local" id="publish_date" name="item[publish_date]"
                                   class="form-control">
                        </div>
                    @else
                        <div>{{date('d/m/Y H:i', strtotime($record->publish_date))}}</div>
                        <div class="mt-3 mb-3">
                            <a href="#" data-enable="#publish_date">Modifica</a>
                        </div>
                        <div>
                            <input style="display:none;" type="datetime-local" disabled="disabled"
                                   id="publish_date" name="item[publish_date]" class="form-control">
                        </div>
                    @endif
                </div>
            </div>
        @endif

        <div class="row mt-4">
            <div class="col-12">
                <!--/span-->
                <div class="form-actions">
                    <button type="submit" class="w-100 btn btn-success">
                        <i class="fa fa-save"></i>
                        Salva
                    </button>
                    <hr/>
                </div>
            </div>
        </div>

        @if($postType->code != 'pages')
            <div class="row">
                <div class="col-12">
                    <label for="categories" class="control-label">Categorie</label>
                    <select name="categories[]" multiple="multiple"
                            data-placeholder="Seleziona le categoria"
                            class="select2 m-b-10 select2-multiple">
                        @foreach($allCat as $cat)
                            <option @if(in_array($cat->id,$catRecord)){{"selected"}}@endif value="{{$cat->id}}">{{$cat->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        @endif

        @include('cms.post.rightmeta')

    </div>
</div>