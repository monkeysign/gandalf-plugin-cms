<h2>Ciao! Sono un Partial</h2>
<p>Ecco le variabili che mi sono state passate:</p>
<pre>{{ var_dump($param) }}</pre>
<div>Logicamente posso accedere anche alle API generiche!</div>
<p>Ad esempio posso accedere al titolo del post dove sono incluso: <strong>{{the_title()}}</strong></p>