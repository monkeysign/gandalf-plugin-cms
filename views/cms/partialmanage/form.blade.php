<?php

use \Ring\Support\Enum\Role;
use \Ring\Support\Enum\Status;

?>
@extends('layout')

@section('content')

    <!-- PAGE CONTENT WRAPPER -->
    <div id="page-wrapper">
        <div class="container-fluid">

            @include('cms.partialmanage.header')

            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h2>Parziali</h2>
                        <p>Questa sezione gestisce i parziali del CMS.</p>
                        <p>Ogni parziale riceve in input un array disponibile sotto la variabile <strong>$param</strong></p>
                        <p>Puoi chiamare un parziale all'interno di un post facendo:</p>
                        <pre>
partial("nomeparziale", "pippo", "baudo", ...)</pre>
                        <p>equivale a chiamare la vista <strong>nomeparziale.blade.php</strong> nel quale saranno disponibilii:</p>
                        <pre>
$param[0] = pippo
$param[1] = baudo
...</pre>
                        <hr/>
                        <form id="form-save" action="{{ path_for('admin.cms.partial.save') }}" method="POST"
                              enctype="multipart/form-data">
                            <input id="item_id_hidden" type="hidden"
                                   value='@if($record->filename){{ $record->filename }}@else{{0}}@endif'
                                   name='item[id]'>
                            <div class="form-body">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Nome del file *</label>
                                            <small><i>( del tipo filename.blade.php )</i></small>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-bookmark"></i>
                                                </div>
                                                <input value="{{$record->filename}}" required
                                                       data-toggle="validator" type="text" name="item[filename]"
                                                       id="code" class="form-control"
                                                       placeholder="filename.blade.php">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>

                                </div>
                                <!--/row-->
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label
                                                    title="Campo obbligatorio per la registrazione"
                                                    class="control-label">Contenuto</label>
                                            <textarea id="content" name="item[content]">{{$record->content}}</textarea>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                            <!--/row-->

                            <!--/span-->
                            <div class="form-actions">
                                <hr style="margin-top: 0px">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salva
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('style')
    <link href="{{asset('assets/plugins/bower_components/toast-master/css/jquery.toast.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/custom-select/custom-select.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/codemirror/lib/codemirror.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/codemirror/theme/dracula.css')}}" rel="stylesheet"
          type="text/css">
@endsection

@section('scripts')
    <script src="{{asset('assets/js/validator.js')}}"></script>
    <script src="{{asset('assets/js/form.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/blockUI/jquery.blockUI.js')}}"></script>

    <!-- Select Plugin JavaScript -->
    <script src="{{asset('assets/plugins/bower_components/custom-select/custom-select.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js')}}"
            type="text/javascript"></script>

    <!-- cedemirror -->
    <script src="{{asset('assets/plugins/bower_components/codemirror/lib/codemirror.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/mode/xml/xml.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/mode/javascript/javascript.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/mode/css/css.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/mode/clike/clike.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/mode/htmlmixed/htmlmixed.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/mode/php/php.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/codemirror/addon/edit/matchbrackets.js')}}"></script>

    <script>
        // Select Choosen
        jQuery(document).ready(function () {
            $(".select-choose").select2();

            var code = CodeMirror.fromTextArea(document.getElementById("content"), {
                theme: 'dracula',
                lineNumbers: true,
                matchBrackets: true,
                mode: "application/x-httpd-php",
                indentUnit: 4,
                indentWithTabs: true
            });

            $('button[type="submit"]').on('click', function () {
                code.save();
                var content = code.getValue(); //textarea text
                $('#content').val(content);
                return true;
            })
        });
    </script>
@endsection