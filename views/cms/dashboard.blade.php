<div class="row">
    <div class="col-12">
        <h2>Contenuti</h2>
    </div>
</div>
<div class="row">
    <div class="col-4">
        <div class="white-box">
            <h3 class="box-title">Pagine Inserite</h3>
            <ul class="list-inline two-part">
                <li><i class="icon-doc text-danger"></i></li>
                <li class="text-right"><span class="">{{$pageCount}}</span></li>
            </ul>
            <a href="{{path_for('admin.cms.post.list', ['code_post_type' => 'pages'])}}" class="btn btn-secondary">Vedi Tutti</a> <a href="{{path_for('admin.cms.post.add', ['code_post_type' => 'pages'])}}" class="btn btn-info">Aggiungi</a>
        </div>
    </div>
    <div class="col-4">
        <div class="white-box">
            <h3 class="box-title">Articoli Inseriti</h3>
            <ul class="list-inline two-part">
                <li><i class="icon-docs text-info"></i></li>
                <li class="text-right"><span class="">{{$postCount}}</span></li>
            </ul>
            <a href="{{path_for('admin.cms.post.list', ['code_post_type' => 'articoli'])}}" class="btn btn-secondary">Vedi Tutti</a> <a href="{{path_for('admin.cms.post.add', ['code_post_type' => 'articoli'])}}" class="btn btn-info">Aggiungi</a>
        </div>
    </div>
</div>
