<?php
//Controller Namespace
const _P_CMS = "\\Plugins\\CMS\\Controllers\\";
// Admin group
router()->group( [ 'middleware' => [ 'web' ] ], function () {
	//auth route
	router()->group( [ 'middleware' => [ 'auth' ], 'prefix' => 'admin/cms' ], function () {

	    // Partial
        router()->group( [ 'prefix' => 'partial' ], function () {
            router()->get('', [ 'as' => 'admin.cms.partial.base' ]);
            router()->get( '/list', [ 'as' => 'admin.cms.partial.list', 'uses' => _P_CMS . 'Partials@listAll' ] );
            router()->get('/add', [ 'as' => 'admin.cms.partial.add', 'uses' => _P_CMS . 'Partials@form' ]);
            router()->get('/{id}', [ 'as' => 'admin.cms.partial.update', 'uses' => _P_CMS . 'Partials@form' ]);
            router()->post('/save', [ 'as' => 'admin.cms.partial.save', 'uses' => _P_CMS . 'Partials@save' ]);
            router()->get('/delete/{id}',[ 'as' => 'admin.cms.partial.delete', 'uses' => _P_CMS . 'Partials@delete' ]);
            router()->post('/delete', [ 'as' => 'admin.cms.partial.deletegroup', 'uses' => _P_CMS . 'Partials@deleteGroup' ]);
        } );

		//Post Type
		router()->group( [ 'prefix' => 'posttype' ], function () {
			router()->get('', [ 'as' => 'admin.cms.posttype.base' ]);
			router()->get( '/list', [ 'as' => 'admin.cms.posttype.list', 'uses' => _P_CMS . 'PostTypes@listAll' ] );
			router()->get('/add', [ 'as' => 'admin.cms.posttype.add', 'uses' => _P_CMS . 'PostTypes@form' ]);
			router()->get('/{id}', [ 'as' => 'admin.cms.posttype.update', 'uses' => _P_CMS . 'PostTypes@form' ]);
			router()->post('/save', [ 'as' => 'admin.cms.posttype.save', 'uses' => _P_CMS . 'PostTypes@save' ]);
			router()->get('/delete/{id}',[ 'as' => 'admin.cms.posttype.delete', 'uses' => _P_CMS . 'PostTypes@delete' ]);
			router()->post('/delete', [ 'as' => 'admin.cms.posttype.deletegroup', 'uses' => _P_CMS . 'PostTypes@deleteGroup' ]);
		} );

		//Singolo Post Type
		router()->group( [ 'prefix' => '{code_post_type}' ], function () {
			router()->get('', [ 'as' => 'admin.cms.singleposttype.base' ]);

			// Tax
			router()->group( [ 'prefix' => 'taxonomy' ], function () {
				router()->get( '/list', [ 'as' => 'admin.cms.taxonomy.list', 'uses' => _P_CMS . 'TaxPostTypes@listAll' ] );
				router()->get('/add', [ 'as' => 'admin.cms.taxonomy.add', 'uses' => _P_CMS . 'TaxPostTypes@form' ]);
				router()->get('/{id}', [ 'as' => 'admin.cms.taxonomy.update', 'uses' => _P_CMS . 'TaxPostTypes@form' ]);
				router()->post('/save', [ 'as' => 'admin.cms.taxonomy.save', 'uses' => _P_CMS . 'TaxPostTypes@save' ]);
				router()->get('/delete/{id}',[ 'as' => 'admin.cms.taxonomy.delete', 'uses' => _P_CMS . 'TaxPostTypes@delete' ]);
				router()->post('/delete', [ 'as' => 'admin.cms.taxonomy.deletegroup', 'uses' => _P_CMS . 'TaxPostTypes@deleteGroup' ]);
			});

			// Categorie
			router()->group( [ 'prefix' => 'categories' ], function () {
				router()->get( '/list', [ 'as' => 'admin.cms.catposttype.list', 'uses' => _P_CMS . 'CatPostTypes@listAll' ] );
				router()->get('/add', [ 'as' => 'admin.cms.catposttype.add', 'uses' => _P_CMS . 'CatPostTypes@form' ]);
				router()->get('/{id}', [ 'as' => 'admin.cms.catposttype.update', 'uses' => _P_CMS . 'CatPostTypes@form' ]);
				router()->post('/save', [ 'as' => 'admin.cms.catposttype.save', 'uses' => _P_CMS . 'CatPostTypes@save' ]);
				router()->get('/delete/{id}',[ 'as' => 'admin.cms.catposttype.delete', 'uses' => _P_CMS . 'CatPostTypes@delete' ]);
				router()->post('/delete', [ 'as' => 'admin.cms.catposttype.deletegroup', 'uses' => _P_CMS . 'CatPostTypes@deleteGroup' ]);
			});

			// Post
			router()->group( [ 'prefix' => 'posts' ], function () {
				router()->get( '/list', [ 'as' => 'admin.cms.post.list', 'uses' => _P_CMS . 'Posts@listAll' ] );
				router()->get('/add', [ 'as' => 'admin.cms.post.add', 'uses' => _P_CMS . 'Posts@form' ]);
				router()->get('/{id}', [ 'as' => 'admin.cms.post.update', 'uses' => _P_CMS . 'Posts@form' ]);
				router()->post('/save', [ 'as' => 'admin.cms.post.save', 'uses' => _P_CMS . 'Posts@save' ]);
				router()->post('/galleryUpload', [ 'as' => 'admin.cms.post.galleryupload', 'uses' => _P_CMS . 'Posts@galleryUpload' ]);
				router()->post('/galleryDelete', [ 'as' => 'admin.cms.post.gallerydelete', 'uses' => _P_CMS . 'Posts@galleryDelete' ]);
				router()->get('/delete/{id}',[ 'as' => 'admin.cms.post.delete', 'uses' => _P_CMS . 'Posts@delete' ]);
				router()->post('/delete', [ 'as' => 'admin.cms.post.deletegroup', 'uses' => _P_CMS . 'Posts@deleteGroup' ]);
			});
		} );
	});
});