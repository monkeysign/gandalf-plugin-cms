<?php
/**
 * Helpers per il plugin CMS
 */

/**
 * @return \Plugins\CMS\Models\Post
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function the_post() {
	if ( container()->has( 'the_post' ) ) {
		return container()->get( 'the_post' );
	} else {
		return false;
	}

}

/**
 * Compila una stringa che contiene Template Blade
 *
 * @param $string
 *
 * @throws Exception
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function buildString( $string, $param = [] ) {
	return view()->build( $string, $param );
}

/**
 * Ritorna il path del media
 *
 * @param $filename
 *
 * @return string
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function the_media( $filename ) {
	return config( 'httpmedia' ) . $filename;
}

/**
 * Torna il meta "title"
 * @return $this|bool|mixed
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function the_title() {
	if ( ! the_post() ) {
		return false;
	}

	return the_post()->meta( 'title' );
}

/**
 * Ritorna il contenuto buildato del content di un post
 * @return bool|string
 * @throws Exception
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function the_content() {
	if ( ! the_post() ) {
		return false;
	}

	return buildString( the_post()->meta( 'content' ), [ 'post' => the_post() ] );
}

/**
 * Ritorna il meta di un post
 * @param $key
 * @return $this|bool|mixed
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function post_meta($key) {
    if ( ! the_post() ) {
        return false;
    }
    return the_post()->meta($key);
}

/**
 * Genera un tag image se il post ha il meta "image"
 *
 * @param string $classes
 *
 * @return bool|string
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function the_image( $classes = '' ) {
	if ( ! the_post() || ! the_post()->meta( 'image' ) ) {
		return false;
	}
	$img = '<img src="' . the_media( the_post()->meta( 'image' ) ) . '" class="' . $classes . '">';

	return $img;
}

/**
 * Genera un link avendo il permalink
 *
 * @param $label
 * @param $perma
 *
 * @return string
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function link_page( $label, $perma ) {
	return '<a href="' . path_for( 'page', [ 'permalink' => $perma ] ) . '">' . $label . '</a>';
}

/**
 * Richiama un partial
 * @param string $name
 * @param array $param
 *
 * @return bool
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function partial($name = '', ...$param){
	if($name)
		view()->render('cms.partials.'.$name, ['param' => $param]);
	else
		return false;
}

/**
 * Torna tutti i file nella cartella media
 * @return array
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function media_gallery(){
	$allFile = glob(config( 'media' ) . '/*', GLOB_BRACE);
	return $allFile;
}